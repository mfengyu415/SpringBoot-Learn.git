package com.feng.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.pojo.TestModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TestMapper extends BaseMapper<TestModel> {

    @Insert("<script>" +
            "INSERT INTO tmp_chenn_test(id,content,createdTime)VALUES" +
            "<foreach collection='testList' item='testModel'   separator=','> " +
            "(#{testModel.id},#{testModel.content},#{testModel.createdTime})" +
            "</foreach> " +
            "</script>")//
    boolean insertBatch(List<TestModel> testList);

}
