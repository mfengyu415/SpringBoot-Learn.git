package com.feng.config;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;

public class SpringMVCConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

            // 或者从配置文件中取static-locations
            registry.addResourceHandler("/**")
                    .addResourceLocations("file:d:\\testfile\\")
                    .setCachePeriod(31556926);


    }
}
