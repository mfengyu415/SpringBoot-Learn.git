package com.feng.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName("tmp_chenn_test")
public class TestModel {
    String id;
    String content;
    Date createdTime;
}
