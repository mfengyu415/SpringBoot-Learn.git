package com.feng.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.feng.pojo.TestModel;
import com.feng.repository.TestMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/Test")
@RestController
public class TestController {

    @Autowired
    private TestMapper testMapper;

    @GetMapping("/select")
    public   void select() {
        QueryWrapper<TestModel> wrapper = new QueryWrapper<>();
        wrapper.eq("id","000000009");
        // 查询全部用户
        // selectList(参数) 这里的参数是一个Wrapper，条件构造器，这里我们先不用，写个null占着
        List<TestModel> users = testMapper.selectList(wrapper);
        /*for (User user : users) {
            System.out.println(user);
        }*/
        System.out.println("s");
        // 语法糖
        users.forEach(System.out::println);
    }
}
