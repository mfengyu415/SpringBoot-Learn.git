package com.feng.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.pojo.TestModel;
import com.feng.repository.TestMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class   TestService  extends ServiceImpl<TestMapper,TestModel> {
}
