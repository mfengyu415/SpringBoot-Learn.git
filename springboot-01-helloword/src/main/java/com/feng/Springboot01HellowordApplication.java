package com.feng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.feng.repository")
@SpringBootApplication
public class Springboot01HellowordApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot01HellowordApplication.class, args);
    }

}
