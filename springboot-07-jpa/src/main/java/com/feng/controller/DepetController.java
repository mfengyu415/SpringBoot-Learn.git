package com.feng.controller;

import com.feng.dao.DepetDao;
import com.feng.model.Depet;
import com.feng.model.Result;
import com.feng.service.DepetService;
import com.feng.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/depet")
public class DepetController {
    @Autowired
    DepetDao depetDao;
    @Autowired
    EntityManager em;
    @Autowired
    DepetService depetService;

    /**
     * 查询列表
     *
     * @param depet
     * @return
     */
    @GetMapping("/list")
    public Result list(Depet depet) {
        log.info("sssssssssssssssssssss");
        System.out.println(depet);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Depet> cq = cb.createQuery(Depet.class);
        Root<Depet> root = cq.from(Depet.class);

        List<Predicate> predicateList = new ArrayList<>();
        StringBuilder bld = new StringBuilder();
        bld.delete(0, bld.length());

        //phone,sale_code,name,type优先
        if (depet.getId() != null) {
            predicateList.add(cb.equal(root.get("id"), depet.getId()));
        }
        if (depet.getName() != null&&!depet.getName().isEmpty()) {
            predicateList.add(cb.equal(root.get("name"), depet.getName()));
        }

        //禁止no where


        Predicate[] p = new Predicate[predicateList.size()];
        cq.where(predicateList.toArray(p));
        cq.orderBy(cb.asc(root.get("id")));

        TypedQuery<Depet> query = em.createQuery(cq);
        return ResultUtil.success("成功", query.getResultList());
    }
    /**
     * 查询列表
     *
     * @param id
     * @return
     */
    @GetMapping("/getmodel/{id}")
    public  Result  getModel(@PathVariable("id") long id) throws Exception {



        if (id==1l)
        {
            throw  new Exception("错误");
        }
        else
        {
            Depet id2 =   depetDao.findById(id).get();
            return ResultUtil.success("成功",id2);
        }

    }
    /**
     * 根据名称查询
     *
     * @param depet
     * @return
     */
    @GetMapping("/listbyname")
    public Result listByName(Depet depet) {
        return
                ResultUtil.success("成功",depetDao.findByName(depet.getName()));
    }

    @PostMapping("/save")
    public Result save(@RequestBody Depet depet) {
        System.out.println(depet);
        Depet de = depetDao.save(depet);
        return  ResultUtil.success("成功");
    }

    @PostMapping("/delete")
    public Result delete(@RequestBody Depet depet) {
        depetDao.delete(depet);
        return  ResultUtil.success("成功");
    }

    @DeleteMapping("/delmodel/{id}")
    public Result delete(@PathVariable("id") long  id) {
        depetDao.deleteById(id);
        return  ResultUtil.success("成功");
    }
    /**
     * 查询列表
     *
     * @param depet
     * @return
     */
    @GetMapping("/getlist")
    public Result getlist(Depet depet) {
        return  ResultUtil.success("成功",depetService.findByCondition(depet));
    }


    /**
     * 查询列表
     *
     * @param depet
     * @return
     */
    @GetMapping("/getdlist")
    public Result getdlist(Depet depet) {
        return  ResultUtil.success("成功",depetDao.getByNameAndDescs(depet));
    }
}
