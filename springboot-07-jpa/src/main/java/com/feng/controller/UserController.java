package com.feng.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.feng.dao.UserAdminDao;
import com.feng.dao.UserDao;
import com.feng.model.User;
import com.feng.model.UserAdmin;
import com.feng.util.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/user")
@Slf4j
@RestController
public class UserController {
    @Autowired
    UserDao userDao;
    @Autowired
    UserAdminDao  userAdminDao;
    @RequestMapping("/getlist")
    public List<User> getList() {
        return userDao.findAll();
    }
    @RequestMapping("/test")
    public String hel() {
        return "22";
    }
    @RequestMapping("/save")
    public void saveuser() {
        User user=new User();
        user.setUserid("userid1");
        user.setUsername("username1");
      userDao.save(user);
    }

    @RequestMapping("/saveadmin")
    public void saveadmin() {
        UserAdmin user=new UserAdmin();
        user.setUserid("userid1");
        user.setUsername("username1");
        userAdminDao.save(user);
    }

    @PostMapping("/login")
    public Map<String,Object> Login(@RequestBody User user)
    {
        Map<String,Object> result = new HashMap<>();
        log.info("用户名:[{}]",user.getUsername());
        log.info("密码:[{}]",user.getPassword());
        try {
            //登录 模拟认证
//            User loginUser =new User();
            //判断login是否为空
            //添加payload载荷
            Map<String, String> map = new HashMap<>();
            map.put("username",user.getUsername());
            map.put("password",user.getPassword());
            //生成token
            String token = JWTUtils.getToken(map);
            //向前端返回code状态码msg信息和token
            result.put("code",200);
            result.put("msg","登陆成功!");
            result.put("token",token);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code",502);
            result.put("msg",e.getMessage());
        }
        return result;
    }

    @PostMapping("/test")
    public Map<String, Object> test() {
        Map<String, Object> map = new HashMap<>();
        try {

            map.put("msg", "验证通过~~~");
            map.put("state", true);
        }  catch (Exception e) {
            e.printStackTrace();
            map.put("state", false);
            map.put("msg", "无效token~~");
        }
        return map;
    }
    @Autowired
    ObjectMapper objectMapper;
    @RequestMapping(value = "/testkey", consumes = "application/x-www-form-urlencoded")
    public String listByKey(HttpServletRequest request) throws JsonProcessingException {
        Map<String, String> params = new HashMap<>();
        Enumeration<String> em = request.getParameterNames();
        while (em.hasMoreElements()) {
            String name = em.nextElement();

                params.put(name, request.getParameter(name));
        }
        return  objectMapper.writeValueAsString(params);
    }
}
