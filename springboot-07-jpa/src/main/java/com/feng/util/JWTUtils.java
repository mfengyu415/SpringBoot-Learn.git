package com.feng.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Map;

public class JWTUtils {
    private static String TOKEN = "gttt@W3e4r";
    /**
     * 生成token
     * @param map  //传入payload
     * @return 返回token
     */
    public static String getToken(Map<String,String> map){
        JWTCreator.Builder builder = JWT.create();
        map.forEach((k,v)->{
            builder.withClaim(k,v);
        });
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR,7*24);//设置过期时间
        builder.withExpiresAt(instance.getTime());
        return builder.sign(Algorithm.HMAC256(TOKEN)).toString();
    }
    /**
     * 验证token
     * @param token
     * @return
     */
    public static void verify(String token){
        JWT.require(Algorithm.HMAC256(TOKEN)).build().verify(token);
    }
    /**
     * 获取token中payload
     * @param token
     * @return
     */
    public static DecodedJWT getToken(String token){
        return JWT.require(Algorithm.HMAC256(TOKEN)).build().verify(token);
    }

    public static void cscreatetoken(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND,600);
        String token = JWT.create()
                .withClaim("username", "gtt")    //设置自定义用户名
                .withClaim("userid", 22)    //设置自定义用户名
                .withClaim("usercode", "code")    //设置自定义用户名
                .withExpiresAt(calendar.getTime())    //设置过期时间
                .sign(Algorithm.HMAC256("gttt!@#$%^&*"));//设置签名 gttt!@#$%^&*为签名的第三部分secret
        System.out.println(token);

    }
    public static void csgettoken(){
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256("gttt!@#$%^&*")).build();
        DecodedJWT decodedJWT = verifier.verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjUzNzgyMjYsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.J2hobFtTIrmovEiDXk9odVi4EHD9ZNwfcSYuAUW9gGI");
        System.out.println("用户名："+decodedJWT.getClaim("username").asString());
        System.out.println("用户id："+decodedJWT.getClaims().get("userid").asString());
        System.out.println("用户code："+decodedJWT.getClaims().get("usercode").asString());
        System.out.println("用户code："+decodedJWT.getClaim("usercode").asString());
        System.out.println("过期时间："+decodedJWT.getExpiresAt());
    }
}
