package com.feng.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZIPUtils {
    /**
     * 使用gzip进行压缩
     */
    public static String compress(String primStr) {
        if (primStr == null || primStr.length() == 0) {
            return primStr;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = null;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(primStr.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (gzip != null) {
                try {
                    gzip.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new sun.misc.BASE64Encoder().encode(out.toByteArray());
    }

    /**
     * 使用gzip进行解压缩
     */
    public static String uncompress(String compressedStr) {
        if (compressedStr == null) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = null;
        GZIPInputStream ginzip = null;
        byte[] compressed = null;
        String decompressed = null;
        try {
            compressed = new sun.misc.BASE64Decoder().decodeBuffer(compressedStr);
            in = new ByteArrayInputStream(compressed);
            ginzip = new GZIPInputStream(in);

            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = ginzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ginzip != null) {
                try {
                    ginzip.close();
                } catch (IOException e) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            try {
                out.close();
            } catch (IOException e) {
            }
        }
        return decompressed;
    }

    public static void main(String[] args) {
        String str =
                "188888888原字符串888888888888888444444444444444444444444原字符串4444443333333333333333332222222222222222288888888888888884444444444444444444444444443333d888888888888888866666";
        str ="{\n" +
                "    \"ret\":1,\n" +
                "    \"data\":{\n" +
                "        \"id\":\"878771\",\n" +
                "        \"name\":\"\\u90b1\\u94ba\",\n" +
                "        \"email\":\"qiuyue02@meicai.cn\",\n" +
                "        \"status\":\"1\",\n" +
                "        \"sale_code\":\"0105242\",\n" +
                "        \"phone\":\"13810044851\",\n" +
                "        \"role\":null,\n" +
                "        \"city_id\":\"1\",\n" +
                "        \"own_city\":\"\",\n" +
                "        \"last_login_time\":\"1626833385\",\n" +
                "        \"last_chpwd_time\":\"1626083272\",\n" +
                "        \"c_t\":\"1570588678\",\n" +
                "        \"u_t\":\"1626083272\",\n" +
                "        \"passport_id\":\"37712504\",\n" +
                "        \"uid\":\"0105242\",\n" +
                "        \"wip_status\":\"0\",\n" +
                "        \"type\":\"1\",\n" +
                "        \"current_city_id\":\"1\",\n" +
                "        \"cas_info\":{\n" +
                "            \"usercode\":\"qiuyue02\",\n" +
                "            \"uid\":\"0105242\",\n" +
                "            \"givenname\":\"\\u90b1\\u94ba\",\n" +
                "            \"telephonenumber\":\"13810044851\",\n" +
                "            \"mail\":\"qiuyue02@meicai.cn\",\n" +
                "            \"orgcode\":\"01010102\",\n" +
                "            \"orgname\":\"\\u5317\\u4eac\\u4e91\\u6749\\u4e16\\u754c\\u4fe1\\u606f\\u6280\\u672f\\u6709\\u9650\\u516c\\u53f8\",\n" +
                "            \"orgshortname\":\"\\u5317\\u4eac\\u4e91\\u6749\\u4e16\\u754c\\u4fe1\\u606f\\u6280\\u672f\\u6709\\u9650\\u516c\\u53f8\",\n" +
                "            \"deptcode\":\"D105326951491\",\n" +
                "            \"deptname\":\"\\u6548\\u80fd\\u7814\\u53d1\\u90e8\",\n" +
                "            \"deptpath\":\"\\u6280\\u672f\\u4f53\\u7cfb|\\u6548\\u80fd\\u4e0e\\u8d28\\u91cf\\u4e2d\\u5fc3|\\u6548\\u80fd\\u7814\\u53d1\\u90e8\",\n" +
                "            \"postname\":\"\\u9ad8\\u7ea7\\u7814\\u53d1\\u5de5\\u7a0b\\u5e08\",\n" +
                "            \"sex\":0,\n" +
                "            \"sextext\":\"1\",\n" +
                "            \"enablestate\":\"2\",\n" +
                "            \"psnclname\":\"\\u6b63\\u5f0f\\u5458\\u5de5\",\n" +
                "            \"pkorg\":\"0001A110000000002S2O\",\n" +
                "            \"pkdept\":\"60136065233067494114\",\n" +
                "            \"updatetime\":\"1615776301\",\n" +
                "            \"username\":\"qiuyue02\",\n" +
                "            \"tgt\":\"ldap\"\n" +
                "        },\n" +
                "        \"token_live_time\":153824\n" +
                "    }\n" +
                "}";
        System.out.println("原字符串：" + str);
        System.out.println("原长度：" + str.length());
        String compress = GZIPUtils.compress(str);
        System.out.println("压缩后字符串：" + compress);
        Base64.Encoder encoder = Base64.getEncoder();
        String jmbass=encoder.encodeToString(compress.getBytes());
        System.out.println("base64加密串：" + jmbass);
        System.out.println("base64加密串：" +jmbass.length());
        System.out.println("压缩后字符串长度：" + compress.length());
        String string = GZIPUtils.uncompress(compress);
        System.out.println("解压缩后字符串：" + string);
        System.out.println("解压缩后字符串：" + str);
    }
}
