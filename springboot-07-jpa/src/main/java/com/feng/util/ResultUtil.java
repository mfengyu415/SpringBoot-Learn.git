package com.feng.util;

import com.feng.model.Result;

public class ResultUtil {
    /**
     * 成功 返回默认码值
     * @param msg
     * @return
     */
    public static Result success(String msg){
        return new Result("200",msg);
    }

    /**
     * 成功  返回自定义码值
     * @param code
     * @param msg
     * @return
     */
    public static Result success(String code, String msg){
        return new Result(code,msg);
    }

    public static Result success(String msg, Object data){
        return  new Result("200",msg,data);
    }

    public static Result fail(String msg){
        return new Result("-1",msg);
    }

    public static Result fail(String code, String msg){
        return new Result(code,msg);
    }
}
