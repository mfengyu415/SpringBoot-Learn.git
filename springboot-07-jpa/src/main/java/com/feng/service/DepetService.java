package com.feng.service;

import com.feng.model.Depet;

import java.util.List;
public interface DepetService {
    List<Depet> findByCondition(Depet depet);
}
