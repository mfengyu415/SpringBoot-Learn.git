package com.feng.service.impl;

import com.feng.dao.DepetDao;
import com.feng.model.Depet;
import com.feng.service.DepetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
@Service
public class DepetImpl implements DepetService {
    @Autowired
    DepetDao depetDao;
    @Override
    public List<Depet> findByCondition(Depet depet) {
        Specification<Depet> query = new Specification<Depet>() {
            @Override
            public Predicate toPredicate(Root<Depet> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(!StringUtils.isEmpty(depet.getId())){
                    predicates.add(criteriaBuilder.equal(root.get("id"),depet.getId()));
                }
                if(!StringUtils.isEmpty(depet.getName())){
                    predicates.add(criteriaBuilder.like(root.get("name"),"%"+depet.getName()+"%"));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        return depetDao.findAll(query);
    }
}
