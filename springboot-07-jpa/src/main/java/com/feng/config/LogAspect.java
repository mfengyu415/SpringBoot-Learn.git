package com.feng.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Slf4j
@Aspect
@Component
public class LogAspect {
    /**
     * execution 表达式的主体
     * 第一个* 代表任意的返回值
     * com.jiuxian aop所横切的包名
     * 包后面.. 表示当前包及其子包
     * 第二个* 表示类名，代表所有类
     * .*(..) 表示任何方法,括号代表参数 .. 表示任意参数
     */
    @Pointcut("execution(* com.feng.controller..*.*(..))")
    private void pointCutLog(){};
    /**
     * 前置通知
     * 记录方法名和请求参数
     * @param joinPoint
     */
    @Before(value = "pointCutLog()")
    public void before(JoinPoint joinPoint){
        // 获取请求头
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        log.info("请求接口路径为:"+request.getRequestURL().toString());
        log.info("请求接口类型为:"+request.getMethod());
        log.info("类方法:"+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
        log.info("请求参数:"+ Arrays.toString(joinPoint.getArgs()));
      /*  ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        HttpServletRequest request = null;
        if (null != requestAttributes) {
            request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String url = request.getScheme()+"://"+ request.getServerName()+request.getRequestURI()+"?"+request.getQueryString();
            System.out.println("7获取全路径（协议类型：//域名/项目名/命名空间/action名称?其他参数）url="+url);
            String url2=request.getScheme()+"://"+ request.getServerName();//+request.getRequestURI();
            System.out.println("协议名：//域名="+url2);
            System.out.println("获取项目名="+request.getContextPath());
            System.out.println("获取参数="+request.getQueryString());
            System.out.println("获取全路径="+request.getRequestURL());
            System.out.println("获取web项目的路径="+request.getSession().getServletContext().getRealPath("/"));//获取web项目的路径
            System.out.println("获取类的当前目录="+this.getClass().getResource("/").getPath());//获取类的当前目录
        }else{
            System.out.println("--------null == requestAttributes-----------");
        }*/
       /* // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        System.out.println("URL:" + request.getRequestURL().toString());
        System.out.println("HTTP_METHOD:" + request.getMethod());
        System.out.println("IP:" + request.getRemoteAddr());
        Enumeration<String> enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String name = enu.nextElement();
            System.out.println("name:" + name + ",value:" + request.getParameter(name));
        }
        System.out.println("当前方法名: "+joinPoint.getSignature().getName());
        System.out.println("当前执行方法参数: "+joinPoint.getArgs()[0]);
        System.out.println("目标对象target: "+joinPoint.getTarget());
        System.out.println("前置增强——保姆做饭");*/
    }

    /**
     * 返回通知
     * 记录接口返回值
     * @param joinPoint
     * @param result
     */
    @AfterReturning(value="pointCutLog()",returning="result")
    public void afterReturning(JoinPoint joinPoint,Object result){
        System.out.println(joinPoint.getSignature().getName()+ ":返回通知生效");
        System.out.println("返回了"+result);
    }

    /**
     * 异常返回
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(value="pointCutLog()",throwing="exception")
    public void afterThrowing(JoinPoint joinPoint, Exception exception){
        System.out.println("出错了"+exception.getMessage());
    }
    //后置通知
//    @After(value = "execution(* com.study.service.UserService.eat(*))")

    /**
     * 后置通知
     * 记录方法名和请求参数
     * @param joinPoint
     */
/*    @After("pointCutLog()")
    public void after(JoinPoint joinPoint){
        System.out.println("当前方法名: "+joinPoint.getSignature().getName());
        System.out.println("当前执行方法参数: "+joinPoint.getArgs()[0]);
        System.out.println("目标对象target: "+joinPoint.getTarget());
        System.out.println("后置增强——保姆洗碗");
    }*/

    /**
     * 环绕通
     * @param joinPoint
     * @return
     * @throws Throwable
     */
   /* @Around("pointCutLog()")
    public Object round(ProceedingJoinPoint joinPoint) throws Throwable { //环绕增强必须要获取ProceedingJoinPoint参数，
        Long startTime = System.currentTimeMillis();
        ApiOperation apiOperation = ((MethodSignature) pjp.getSignature()).getMethod().getAnnotation(ApiOperation.class);
        // 获取request对象
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        Object ret = pjp.proceed();
        Long endTime = System.currentTimeMillis();
        TjapiLogVo tjapiLogVo = getTjapiLogVo(request, Objects.nonNull(apiOperation) ? apiOperation.value() : "", endTime - startTime, JSON.toJSONString(ret));
        // 添加日志
        addLog(tjapiLogVo);
        log.info("响应数据耗时{}:响应数据{}", endTime - startTime, JSON.toJSONString(ret));
        return ret;
    }*/

    /**
     * 环绕通
     * @param joinPoint
     * @return
     * @throws Throwable
     */
/*    @Around("pointCutLog()")
    public Object round(ProceedingJoinPoint joinPoint) throws Throwable { //环绕增强必须要获取ProceedingJoinPoint参数，
        System.out.println("当前方法名: "+joinPoint.getSignature().getName());
        System.out.println("当前执行方法参数: "+joinPoint.getArgs()[0]); 目标对象没参数不可获取
        System.out.println("目标对象target: "+joinPoint.getTarget());
        System.out.println("环绕增强——保安开门"); //目标方法执行前
        Object proceed = joinPoint.proceed(); //执行目标方法
        System.out.println("环绕增强——保安关门"); //目标方法执行后
        return proceed; //返回目标方法执行结果
    }*/

    //返回通知，方法正常返回则生效




  /*  @Pointcut("execution(public * com.feng.controller..*.*(..))")
    public void webLog() {

    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        System.out.println("URL:" + request.getRequestURL().toString());
        System.out.println("HTTP_METHOD:" + request.getMethod());
        System.out.println("IP:" + request.getRemoteAddr());
        Enumeration<String> enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String name = enu.nextElement();
            System.out.println("name:" + name + ",value:" + request.getParameter(name));
        }
    }

    @AfterReturning(returning="ret",pointcut="webLog()")
    public void doAfterReturning(Object ret)throws Throwable {
        //处理完请求，返回内容
        System.out.println("RESPONSE："+ret);
    }*/
/*
    @AfterReturning(returning="ret",pointcut="webLog()")
    public void doAfterReturning(Object ret)throws Throwable {
        //处理完请求，返回内容
        System.out.println("RESPONSE："+ret);
    }*/
}
