package com.feng.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {

    private static final long serialVersionUID = -8124255851175781743L;
    /**
     * 0成功  -1失败
     */
    private String code;

    private String msg;

    private Object data;

    public Result(String code, String msg){
        this.code = code;
        this.msg = msg;
    }
}

