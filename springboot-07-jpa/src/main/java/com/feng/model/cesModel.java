package com.feng.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class cesModel implements Serializable {

    public  String code;
    public  String name;
}
