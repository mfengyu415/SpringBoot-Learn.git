package com.feng.interceptor;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.feng.model.Result;
import com.feng.util.JWTUtils;
import com.feng.util.ResultUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JWTInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Authorization");
        Result result = new Result();
        try {
            JWTUtils.verify(token);
            DecodedJWT token1 = JWTUtils.getToken(token);
            System.out.println("token"+ token);
            System.out.println("token的用户名"+ token1.getClaim("username").asString());
            return true;
        } catch (TokenExpiredException e) {
            result = ResultUtil.fail("Token已经过期");
        } catch (SignatureVerificationException e){
            result =   ResultUtil.fail("签名错误");
        } catch (AlgorithmMismatchException e){
            result =   ResultUtil.fail("加密算法不匹配");
        } catch (Exception e) {
            e.printStackTrace();
            result =   ResultUtil.fail("无效token");
        }
        String json = new ObjectMapper().writeValueAsString(result);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().println(json);
        return false;
    }
}
