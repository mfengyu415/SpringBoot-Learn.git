package com.feng.dao;

import com.feng.model.Depet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepetDao extends CrudRepository<Depet, Long>, JpaRepository<Depet,Long>, JpaSpecificationExecutor<Depet> {
  /*  List<Depet> findById(long id);*/
    List<Depet> findByName(String name);
    @Query("select u from Depet u where u.name = ?1 and u.desc = ?2")
    List<Depet>  getByNameAndDescs(Depet depet);
}
