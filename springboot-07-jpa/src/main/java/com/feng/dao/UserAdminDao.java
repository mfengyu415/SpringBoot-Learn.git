package com.feng.dao;

import com.feng.model.UserAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAdminDao  extends JpaRepository<UserAdmin,Long> {
}
