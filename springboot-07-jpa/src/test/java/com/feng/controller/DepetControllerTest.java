package com.feng.controller;

import com.feng.model.Depet;
import com.feng.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@Slf4j
@SpringBootTest
class DepetControllerTest {
    @Autowired
    DepetController controller;

    @Test
    void list() {
        Depet depet = new Depet();
//        depet.setId(1l);
        depet.setName("cs");
//        List<Depet> list = controller.list(depet);
//        System.out.println(list);
    }

    @Test
    void listByName() {
        Depet depet = new Depet();
//        depet.setId(1l);
        depet.setName("开发部");
        Result result = controller.listByName(depet);
        System.out.println(result.getData());
    }

    @Test
    void add() {
        Depet depet = new Depet();
        depet.setId(3l);
        depet.setCode("设计code1");
        depet.setName("设计部");
        depet.setDesc("负责设置2");
        depet.setCreatetime(new Date());
        Result save = controller.save(depet);
        System.out.println(save.getData());
    }

    @Test
    void delete() {
        Depet depet = new Depet();
        depet.setId(3l);
        controller.delete(depet);

    }

    @Test
    void getlist() {
        Depet depet = new Depet();
//        depet.setId(1l);
        depet.setName("cs");
        Result getlist = controller.getlist(depet);
        System.out.println(getlist.getData());
    }

    @Test
    void getdlist() {
        Depet depet = new Depet();
        depet.setId(2l);
        depet.setDesc("负责测试");
        Result getlist = controller.getlist(depet);
        System.out.println(getlist.getData());
    }

    @Test
    void getModel() {
  /*      Result model = controller.getModel(1l);
        System.out.println(model.getData());*/
    }
    final static String SYSTEM_KEY = "system_key";
    @Test
    void condition() {

String ss="system_key";

if (ss==SYSTEM_KEY)
{
    System.out.println("");
}
      /*  name==SYSTEM_KEY*/
    /*    assertEquals("1","1");
        assertTrue(1==1);

        assertFalse("false",2==1);
        assertTrue(1==1);
        assertNull(null);
        assertSame(2,4-2);
        Assertions.assertThat( "111", Assertions.equalTo("22") );*/
    }

    @Test
    void teststring() {
        String t1=null,t2="",t3=" ",t4="    " ,t5=" 1";
        System.out.println(StringUtils.isEmpty(t1));
        System.out.println(StringUtils.isEmpty(t2));
        System.out.println(StringUtils.isEmpty(t3));
        System.out.println(StringUtils.isEmpty(t4));
        System.out.println(StringUtils.isEmpty(t5));
        System.out.println(StringUtils.isBlank(t1));
        System.out.println(StringUtils.isBlank(t2));
        System.out.println(StringUtils.isBlank(t3));
        System.out.println(StringUtils.isBlank(t4));
        System.out.println(StringUtils.isBlank(t5));
        System.out.println(StringUtils.isAnyBlank(t1,t2,t3,t4,t5));

    }
    @Test
    void testUtils() {
     /*   UserInfoResp userInfoResp = null;
        if (userInfoResp == null || StringUtils.isBlank(userInfoResp.getId())) {
            log.info("kong");
        }
        Map<String, Map<String, String>> roles=null;
        if (roles!=null&&!roles.isEmpty())
        {
            log.info("null kong");
        }
        if(CollectionUtils.isEmpty(roles)){
            log.info("======集合类空的");
        }

        roles=new HashMap<>();
        if(CollectionUtils.isEmpty(roles)){
            log.info("======集合类空的1111111");
        }

        if (roles!=null)
        {
            log.info("map不是空的e"+roles);
        }
        if (roles!=null&&!roles.isEmpty())
        {
            log.info("null kong");
        }
        Map<String, String> put1 = new HashMap<String, String>();
        put1.put("kkey1","val1");
        roles.put("key",put1);
        if (roles!=null&&!roles.isEmpty())
        {
            log.info("=====Map不是空的");
        }
        Map<String, String> sss = roles.get("sss");
        System.out.println("===========ssss:"+sss);

        List<RedisSysConfigDto>  filterList;*/


    }

}
