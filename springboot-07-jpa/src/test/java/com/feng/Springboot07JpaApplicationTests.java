package com.feng;

import com.feng.model.cesModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@SpringBootTest
class Springboot07JpaApplicationTests {

    @Test
    void contextLoads() {
        log.info("111");
        final StringBuilder bld = new StringBuilder();

        bld.append("当前处理: ").append("system key: ");

        System.out.println(bld);
        bld.delete(0, bld.length());

        System.out.println(bld);
        int i = 1;
        switch (i) {
            case 0:
            case 1:
                System.out.println("01");
                break;
            case 2:
                System.out.println("2");
                break;
        }
    }

    @Test
    void testmy() {
        long now = System.currentTimeMillis();
        System.out.println(now);
        long c = 60 * 60 * 24;
   /*     System.out.println(c);
        System.out.println(now - c);*/

     /*   String x= stampToDate( String.valueOf(now - c));
        String n= stampToDate( String.valueOf(now));*/

        System.out.println(stampToDate(String.valueOf(c)));
        System.out.println(stampToDate(String.valueOf(now)));
        String s = stampToDate(String.valueOf(now - c));

        System.out.println(s);

//        $filterTime = time() - $this->ldapConfig['scanPeriod'];
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);

        res = simpleDateFormat.format(date);
        Integer.parseInt("1");
        return res;
    }

    @Test
    void testut() {
        cesModel model = new cesModel();
        model = new cesModel();
        model.code = "01";
        model.name = "name1";
        List<cesModel> models = new ArrayList();
        models.add(model);
        List<cesModel> accessGroups = models.stream()
                .filter(item -> item.getCode().equals("name1"))
                .collect(Collectors.toList());
        System.out.println(accessGroups);
        if (!CollectionUtils.isEmpty(accessGroups)) {
            System.out.println("不是空的accessGroups");
        } else {
            System.out.println("空的！");
        }
        String s = "5.0";
 /*       Integer num = NumberUtils.parseNumber(s, Integer.class);
        System.out.println(num);
        int i = Integer.parseInt(s);*/

       /*int n = 0;
        n = Integer.valueOf(s).intValue();
        System.out.println("Integer.parseInt(str) : " + n);*/

        System.out.println((int)NumberUtils.toDouble(s));
    }

    @Test
    void testutmap() {
        Map<String,String> map=new HashMap<>();
        map.put("code1","1");
        map.put("sta","2");
        System.out.println(map.get("code1"));
      /*  if (map.get("code11").equals("1"))
        {
            System.out.println("11111111");
        }
       */
        System.out.println(map.get("level"));
        System.out.println(NumberUtils.toDouble(map.get("level")));
        String.valueOf((int)NumberUtils.toDouble(map.get("level")));

    }

}
