package com.feng;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.feng.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class Springboot08RedisApplicationTests {

    // 这就是之前 RedisAutoConfiguration 源码中的 Bean
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    StringRedisTemplate redis;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    RedisUtil redisUtil;

    @Test
    void contextLoads() {
        /** redisTemplate 操作不同的数据类型，API 和 Redis 中的是一样的
         * opsForValue 类似于 Redis 中的 String
         * opsForList 类似于 Redis 中的 List
         * opsForSet 类似于 Redis 中的 Set
         * opsForHash 类似于 Redis 中的 Hash
         * opsForZSet 类似于 Redis 中的 ZSet
         * opsForGeo 类似于 Redis 中的 Geospatial
         * opsForHyperLogLog 类似于 Redis 中的 HyperLogLog
         */

        // 除了基本的操作
        // ，常用的命令都可以直接通过redisTemplate操作，比如事务……

        // 和数据库相关的操作都需要通过连接操作
        //RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        //connection.flushDb();

        redisTemplate.opsForValue().set("key", "呵呵");
        System.out.println(redisTemplate.opsForValue().get("key"));
    }
    @Test
    public void test() throws JsonProcessingException {
        User user = new User("圆","20");
        // 使用 JSON 序列化
//        String s = new ObjectMapper().writeValueAsString(user);
//        String s=mapper.writeValueAsString(user);
//        redisTemplate.opsForValue().set("user", s);
        // 这里直接传入一个对象
        redisTemplate.opsForValue().set("user", user);
        System.out.println(redisTemplate.opsForValue().get("user"));

        User user2 = new User("2","20");
        redisTemplate.opsForValue().set("user2", user2);
        System.out.println(redisTemplate.opsForValue().get("user"));

      String s=mapper.writeValueAsString(user2);
//       redisTemplate.opsForValue().set("user", s);
        redisUtil.set("uu",s);
        System.out.println(redisUtil.get("uu"));



        redisTemplate.opsForValue().set("key", "呵呵");

        redisTemplate.opsForValue().set("my", s);
        System.out.println(redisTemplate.opsForValue().get("my"));

        redisUtil.set("my1",user);
        System.out.println(redisUtil.get("my1"));

        redis.opsForValue().set("my2",s);
        System.out.println(redis.opsForValue().get("my2"));
    }
    @Test
    public void testjson() throws JsonProcessingException {
        mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        User user = new User("圆","20");
        String s=mapper.writeValueAsString(user);

        redisTemplate.opsForValue().set("my", s);
        System.out.println(redisTemplate.opsForValue().get("my"));

        redisUtil.set("my1",s);
        System.out.println(redisUtil.get("my1"));

        redis.opsForValue().set("my2",s);
        System.out.println(redis.opsForValue().get("my2"));

    }
    @Test
    public void testObj() throws JsonGenerationException, JsonMappingException, IOException {
        User user = new User("圆","20");

        mapper.writeValue(new File("D:/test.txt"), user); // 写到文件中
        // mapper.writeValue(System.out, user); //写到控制台

        String jsonStr = mapper.writeValueAsString(user);
        System.out.println("对象转为字符串：" + jsonStr);

        byte[] byteArr = mapper.writeValueAsBytes(user);
        System.out.println("对象转为byte数组：" + byteArr);

        User userDe = mapper.readValue(jsonStr, User.class);
        System.out.println("json字符串转为对象：" + userDe);

        User useDe2 = mapper.readValue(byteArr, User.class);
        System.out.println("byte数组转为对象：" + useDe2);

    }
    @Test
    public void testObj1() throws JsonGenerationException, JsonMappingException, IOException {
          String hkey="admin_user_10760_web";
          String key="092b49993d662c9e3cdc0e1d5ee8758c";
          redisTemplate.opsForHash().put(hkey,key,"1123sadasdasd");
        redisTemplate.expire(hkey, 15000, TimeUnit.SECONDS);
        Object o = redisTemplate.opsForHash().get(hkey, key);
        System.out.println("cuncde：" + o);
        Object o1 = redisTemplate.getExpire(hkey, TimeUnit.SECONDS);
        System.out.println("cuncde1：" + o1);
    }

    @Test
    public void testObj2() throws JsonGenerationException, JsonMappingException, IOException {
        String key="sys_config";
        String s = redis.opsForValue().get(key);
        System.out.println("cuncde：" + s);

        Object o = redisTemplate.opsForValue().get(key);
        System.out.println("cuncde：" + o);

    }
}
