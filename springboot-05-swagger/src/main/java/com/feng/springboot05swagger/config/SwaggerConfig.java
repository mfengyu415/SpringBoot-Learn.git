package com.feng.springboot05swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /*访问 /swagger-ui.html*/
    static final String BASE_PACKAGE = "com.feng.springboot05swagger.controller";
    static final String TITLE = "auth-api-ext";
    static final String VERSION = "1.0";
    static final String NAME = "qiuyue02";
    static final String SITE = "auth.yunshanmeicai.com";
    static final String EMAIL = "qiuyue02@meicai.cn";

    /**
     * swagger2 配置
     *
     * @return
     */
    @Bean
    public Docket swagger2Config() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                        .title(TITLE)
                        .description("")
                        .version(VERSION)
                        .contact(new Contact(NAME, SITE, EMAIL))
                        .build());
    }
}
