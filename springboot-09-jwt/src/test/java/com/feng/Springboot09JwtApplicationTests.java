package com.feng;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;

@SpringBootTest
class Springboot09JwtApplicationTests {

    @Test
    void contextLoads() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND,5*600);
        String token = JWT.create()
                .withClaim("username", "gtt")    //设置自定义用户名
                .withClaim("userid", 22)    //设置自定义用户名
                .withClaim("usercode", "code")    //设置自定义用户名
                .withExpiresAt(calendar.getTime())    //设置过期时间
                .sign(Algorithm.HMAC256("gttt!@#$%^&*"));//设置签名 gttt!@#$%^&*为签名的第三部分secret
        System.out.println(token);

    }
    @Test
    void  test(){
        String dd="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjY3Njg1NjgsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.kWm3OXxuWU1MJJRHqfC76Ta6PsYkC79_bShMgs9ySmQ";
        String de="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjY3Njg1MTUsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.1ONAXjg1UcKNPIKshqpQA7FuYSOQiQ8D6N9Z5OROG_M";
/*        JWTVerifier verifier = JWT.require(Algorithm.HMAC256("gttt!@#$%^&*")).build();
        DecodedJWT decodedJWT = verifier.verify(de);
        System.out.println("用户名："+decodedJWT.getClaim("username").asString());
        System.out.println("用户id："+decodedJWT.getClaims().get("userid").asInt());
        System.out.println("用户code："+decodedJWT.getClaims().get("usercode").asString());
        System.out.println("用户code："+decodedJWT.getClaim("usercode").asString());
        System.out.println("过期时间："+decodedJWT.getExpiresAt().toString());*/


        System.out.println(DigestUtils.md5DigestAsHex(dd.getBytes(StandardCharsets.UTF_8)));
        System.out.println(DigestUtils.md5DigestAsHex(de.getBytes(StandardCharsets.UTF_8)));
        System.out.println(DigestUtils.md5DigestAsHex((dd+de).getBytes(StandardCharsets.UTF_8)));

        String dss="e18432a979833c58b63b621c67cb1607";

    }


    @Test
    void testc() {
       /* Map<String, String> map = new HashMap<>();
        map.put("username",user.getUsername());
        map.put("password",user.getPassword());
        //生成token
        String token = JWTUtils.getToken(map);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND,5*600);
        String token = JWT.create()
                .withClaim("username", "gtt")    //设置自定义用户名
                .withClaim("userid", 22)    //设置自定义用户名
                .withClaim("usercode", "code")    //设置自定义用户名
                .withExpiresAt(calendar.getTime())    //设置过期时间
                .sign(Algorithm.HMAC256("gttt!@#$%^&*"));//设置签名 gttt!@#$%^&*为签名的第三部分secret
        System.out.println(token);*/

    }
}
