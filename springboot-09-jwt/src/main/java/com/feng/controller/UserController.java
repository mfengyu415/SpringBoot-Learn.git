package com.feng.controller;

import com.feng.pojo.User;
import com.feng.util.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @GetMapping("/login")
    public Map<String,Object> Login(User user)
    {
        Map<String,Object> result = new HashMap<>();
        log.info("用户名:[{}]",user.getUserName());
        log.info("密码:[{}]",user.getPassWord());
        try {
            //登录 模拟认证
            User loginUser =new User();
            //判断login是否为空
            //添加payload载荷
            Map<String, String> map = new HashMap<>();
            map.put("username",loginUser.getUserName());
            map.put("password",loginUser.getPassWord());
            //生成token
            String token = JWTUtils.getToken(map);
            //向前端返回code状态码msg信息和token
            result.put("code",200);
            result.put("msg","登陆成功!");
            result.put("token",token);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code",502);
            result.put("msg",e.getMessage());
        }
        return result;
    }

    @PostMapping("/test")
    public Map<String, Object> test() {
        Map<String, Object> map = new HashMap<>();
        try {

            map.put("msg", "验证通过~~~");
            map.put("state", true);
        }  catch (Exception e) {
            e.printStackTrace();
            map.put("state", false);
            map.put("msg", "无效token~~");
        }
        return map;
    }
}
