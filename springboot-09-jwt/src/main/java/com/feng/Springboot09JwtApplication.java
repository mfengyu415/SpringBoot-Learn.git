package com.feng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot09JwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot09JwtApplication.class, args);
    }

}
