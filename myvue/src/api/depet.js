import request from './api'
export default {
  getList (data) {
    return request({
      method: 'get',
      url: '/api/depet/list',
      params: data
    })
  },
  save (data) {
    return request({
      method: 'post',
      url: '/api/depet/save',
      data: data
    })
  },
  getModel (id) {
    return request({
      url: '/api/depet/getmodel/' + id,
      method: 'get'
    })
  },
  // 删除测试业务
  delModel (id) {
    return request({
      url: '/api/depet/delmodel/' + id,
      method: 'delete'
    })
  }
}
