import depetApi from './depet'
import loginApi from './login'
import authApi from './auth'
export {
  depetApi,
  loginApi,
  authApi
}
