import axios from 'axios'
export default {
  login (data) {
    return axios({
      method: 'post',
      url: '/api/user/login',
      data: data
    })
  },
  getModel (id) {
    return axios({
      url: '/api/depet/getmodel/' + id,
      method: 'get'
    })
  },
  // 删除测试业务
  delModel (id) {
    return axios({
      url: '/api/depet/delmodel/' + id,
      method: 'delete'
    })
  }
}
