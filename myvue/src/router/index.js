import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
// 导入组件
import Main from '@/views/Main'
import Login from '@/views/Login'
// 导入子模块
import UserList from '@/views/user/List'
import UserProfile from '@/views/user/Profile'

import NotFound from '@/views/NotFound'

import DepetList from '@/views/depet/List'
import CreateDepet from '@/views/depet/CreateDepet'
import DList from '@/views/depet/DList'
import test1 from '@/views/depet/test1'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/main',
      name: 'Main',
      component: Main,
      //  写入子模块
      children: [
        {
          path: '/user/profile/:id',
          name: 'UserProfile',
          component: UserProfile,
          props: true
        }, {
          path: '/user/list',
          component: UserList
        }, {
          path: '/depet/createdepet',
          name: 'CreateDepet',
          component: CreateDepet
        }, {
          path: '/depet/list',
          name: 'DepetList',
          component: DepetList
        }, {
          path: '/depet/dlist',
          name: 'DList',
          component: DList
        }, {
          path: '/depet/test1',
          name: 'test1',
          component: test1
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/goHome',
      redirect: '/main'
    },
    {
      path: '*',
      component: NotFound
    }
  ]
})
