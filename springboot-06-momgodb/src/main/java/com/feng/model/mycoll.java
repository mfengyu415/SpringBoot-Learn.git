package com.feng.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
@Getter
@Setter
@ToString
@Document(collection = "mycoll")
public class mycoll  implements Serializable {

    private static final long serialVersionUID = -8572793132279387388L;

    private String userid;

    private String username;
}
