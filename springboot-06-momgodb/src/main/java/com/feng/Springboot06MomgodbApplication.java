package com.feng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot06MomgodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot06MomgodbApplication.class, args);
    }

}
