#!/bin/sh
source /etc/profile
base_dir=$(cd "$(dirname "$0")"; pwd)
jar_file=`ls $base_dir -t| grep "^springboot-12-assembly.*\.jar$"`
server_name="springboot-12-assembly"
launcher_daemon_out="server.out"

if [ ! -f "$jar_file" ]
then
    echo "can not found jar file , failed to start server! "
    exit 1
fi

pid=`ps -ef | grep "serverName=$server_name" | grep -v "grep" | awk '{print $2}'`

if [ "$pid" = "" ];then
     nohup java -DserverName=$server_name -Dbasedir=$base_dir -Djava.security.egd=file:/dev/./urandom -Dloader.path=. $jvm_args -jar $jar_file >"$launcher_daemon_out" 2>&1 < /dev/null  &
else
    echo "$server_name is running"
fi
