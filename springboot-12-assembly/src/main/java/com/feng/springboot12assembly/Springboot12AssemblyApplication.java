package com.feng.springboot12assembly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot12AssemblyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot12AssemblyApplication.class, args);
    }

}
