package com.feng.controller;

import com.feng.dto.LoginRequestDto;
import com.feng.dto.RespCode;
import com.feng.dto.RetWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
public class LoginController {

    final static int RET_OK = 1;
    final static int RET_FAIL = -1;
    @RequestMapping(value = "/adminuser/login", consumes = "application/json")
    public ResponseEntity<RetWrapper<?>> login(@RequestBody LoginRequestDto loginRequest) {

        //debug
        log.info("request: ");
        log.info(loginRequest.toString());

//        return    new ResponseEntity<>(new RetWrapper<>("", RET_OK), HttpStatus.OK);
        RespCode c = RespCode.COMMON_WRONG_PARAMS;
        return new ResponseEntity<>(
                new RetWrapper<>(
                        c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);

/*
        //入参 & 清洗
        final String email = loginSrv.getEmail(loginRequest.getEmail());
        final String identity = loginSrv.getIdentity(loginRequest.getEmail());
        final String device = loginSrv.getDevice(loginRequest.getDevice());
        final Map<String, String> extraData = loginSrv.getExtraData(loginRequest);

        try {
            LoginResponseDto res = loginSrv.login(identity, email, loginRequest.getPassword(), device, extraData);
            //登录成功
            AuthEvent e = AuthEvent.getUserLoginEvent();
            e.setUser(email);
            e.setEmail(email);
            asyncHelper.produce(e, "auth_event_user_login:" + email);
            return new ResponseEntity<>(new RetWrapper<>(res, RET_OK), HttpStatus.OK);
        } catch (AuthException.PasswordWrongException ex) {
            //密码错误
            AuthEvent e = AuthEvent.getWrongPasswordEvent();
            e.setUser(email);
            e.setEmail(email);
            asyncHelper.produce(e, "auth_event_wrong_password:" + email);
            RespCode c = RespCode.PASSWORD_ERROR;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.PasswordWrongWarningException ex) {
            //密码错误达到告警条件
            AuthEvent e1 = AuthEvent.getWrongPasswordEvent();
            e1.setUser(email);
            e1.setEmail(email);
            AuthEvent e2 = AuthEvent.getWrongPasswordWarningEvent();
            e2.setUser(email);
            e2.setEmail(email);
            asyncHelper.produce(e1, "auth_event_wrong_password:" + email);
            asyncHelper.produce(e2, "auth_event_wrong_password_warning: " + email);

            RespCode c = RespCode.PASSWORD_ERROR_WARNING;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            String.format(c.getDetail(), ex.getCurrentTimes(), ex.getLastTimes()),
                            RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.AccountLockedException ex) {
            AuthEvent e = AuthEvent.getUserLockedEvent();
            e.setUser(email);
            e.setEmail(email);
            asyncHelper.produce(e, "auth_event_user_locked:" + email);
            //账号锁定
            RespCode c = RespCode.USER_TEMPORY_LOCKED;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.PasswordWeakException ex) {
            //to-do: 密码错误事件
            RespCode c = RespCode.USER_PASSWORD_WEAK;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.PasswordExpiredException ex) {
            //to-do: 密码过期事件
            RespCode c = RespCode.USER_NO_CHANGE_PWD_NINETY;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.UserNotExistedException | AuthException.HrInfoNotExistedException ex) {
            //to-do: 用户不存在事件
            RespCode c = RespCode.PASSWORD_ERROR;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.UserForbiddenException ex) {
            //to-do: 禁登录用户尝试登录
            RespCode c = RespCode.USER_LOCKED;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.UserFirstLoginException ex) {
            //to-do: 首次登录事件
            RespCode c = RespCode.USER_LOGIN_FIRST;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        } catch (AuthException.UserPasswordNullException ex) {
            RespCode c = RespCode.COMMON_WRONG_PARAMS;
            return new ResponseEntity<>(
                    new RetWrapper<>(
                            c.getDetail(), RET_FAIL, c.getCode()), HttpStatus.OK);
        }*/
    }

    @GetMapping("/healthcheck/index")
    public void index() {
        //do nothing
        return;
    }
}
