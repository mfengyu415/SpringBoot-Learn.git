package com.feng.dto;

import lombok.Getter;

@Getter
public enum RespCode {
    SUCCESS(1, "成功"),
    ERROR(0, "系统错误"),
    COMMON_WRONG_PARAMS(1001,"参数错误"),
    USER_NOT_EXIST(2001,"用户不存在"),
    PASSWORD_ERROR(2002,"用户名或者密码错误"),
    PASSWORD_ERROR_WARNING(2002,"密码错误已达%d次，再错%d次账户将被冻结"), //这个code和PASSWORD_ERROR一样
    USER_LOCKED(2003, "用户被禁用，请联系管理员"),
    USER_NO_CHANGE_PWD_NINETY(2030,"密码已超过90天请修改，否则无法登陆"),
    USER_PASSWORD_LENGTH(2031,"密码长度至少8位"),
    USER_PASSWORD_RULE(2032, "密码至少同时包含大写字母、小写字母、数字和特殊字符中的三种"),
    USER_PASSWORD_WEAK(2033, "当前密码是弱密码,不允许登陆"),
    USER_LOGIN_FIRST(2034, "初次登录，请先重置密码"),
    USER_TEMPORY_LOCKED(2019, "密码错误次数过多,请重置密码后重试"),
    LENGTH_LIMIT(901, "消息内容过长"),
    SESSIONOUT(400, "请重新登录"),
    RESOURCE_EXIST(800, "资源已经存在"),
    RESOURCE_NONEXIST(801, "资源不存在"),
    ACCESS_FORBID(802, "无权限访问"),
    REQUEST_LIMIT(999, "资源限流");

    private int code;
    private String detail;

    RespCode(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }
}
