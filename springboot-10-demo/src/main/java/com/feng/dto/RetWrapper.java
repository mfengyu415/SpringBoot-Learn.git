package com.feng.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetWrapper<T> {

    final T data;

    final int ret;

    @JsonInclude(JsonInclude.Include.NON_NULL) //如果为null则不参与json序列化
            Integer code = null;

    public RetWrapper(T data, int ret) {
        this.data = data;
        this.ret = ret;
    }
    public RetWrapper(T data, int ret, int code) {
        this.data = data;
        this.ret = ret;
        this.code = code;
    }
}
