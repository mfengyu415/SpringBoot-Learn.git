package com.feng.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true) //必须，否则传入值多一个字段json转换就抛异常，登录就失败
public class LoginRequestDto {
    String email;
    String password;
    String accept;
    String systemKey;
    String systemVersion;
    String device;
    String deviceId;
    String deviceInfo;
    String realIp;
}
