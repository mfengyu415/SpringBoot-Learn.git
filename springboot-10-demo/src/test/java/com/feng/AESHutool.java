package com.feng;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

public class AESHutool {
    public static void main(String[] args) throws IOException {
       /* String content = "test中文";*/
        String content="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjY3Njg1NjgsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.kWm3OXxuWU1MJJRHqfC76Ta6PsYkC79_bShMgs9ySmQ";
      /*  // 随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        // 构建
        AES aes = SecureUtil.aes(key);
        // 加密
        byte[] encrypt = aes.encrypt(content);
        // 解密
        byte[] decrypt = aes.decrypt(encrypt);
        // 加密为16进制表示
        String encryptHex = aes.encryptHex(content);
        System.out.println(encryptHex);
        // 解密为字符串
        String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println(decryptStr);*/


        // BASE64加密
        BASE64Encoder encoder = new BASE64Encoder();
        String data = encoder.encode(content.getBytes());
        System.out.println("BASE64加密：" + data);

        // BASE64解密
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] bytes = decoder.decodeBuffer(data);
        System.out.println("BASE64解密：" + new String(bytes));
    }
}
