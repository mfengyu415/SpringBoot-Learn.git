package com.feng;

import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * 采用MD5加密解密
 * @author tfq
 * @datetime 2011-10-13
 */
public class MD5Util {

    /***
     * MD5加码 生成32位md5码
     */
    public static String string2MD5(String inStr){
        MessageDigest md5 = null;
        try{
            md5 = MessageDigest.getInstance("MD5");
        }catch (Exception e){
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++){
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();

    }

    /**
     * 加密解密算法 执行一次加密，两次解密
     */
    public static String convertMD5(String inStr){

        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++){
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        return s;

    }

    // 测试主函数
    public static void main(String args[]) {
        String s = new String("tangfuqiang");
        System.out.println("原始：" + s);
        System.out.println("MD5后：" + string2MD5(s));
        System.out.println("加密的：" + convertMD5(s));
        System.out.println("解密的：" + convertMD5(convertMD5(s)));
        String dd="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjY3Njg1NjgsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.kWm3OXxuWU1MJJRHqfC76Ta6PsYkC79_bShMgs9ySmQ";
        String de="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyY29kZSI6ImNvZGUiLCJleHAiOjE2MjY3Njg1MTUsInVzZXJpZCI6MjIsInVzZXJuYW1lIjoiZ3R0In0.1ONAXjg1UcKNPIKshqpQA7FuYSOQiQ8D6N9Z5OROG_M";
        System.out.println(DigestUtils.md5DigestAsHex(dd.getBytes(StandardCharsets.UTF_8)));
        System.out.println(DigestUtils.md5DigestAsHex(de.getBytes(StandardCharsets.UTF_8)));
        System.out.println(DigestUtils.md5DigestAsHex((dd+de).getBytes(StandardCharsets.UTF_8)));
        System.out.println("MD5后：" + string2MD5(dd));
        System.out.println("解密的：" + convertMD5(convertMD5("e18432a979833c58b63b621c67cb1607")));
        String dss="e18432a979833c58b63b621c67cb1607";
    }
}
