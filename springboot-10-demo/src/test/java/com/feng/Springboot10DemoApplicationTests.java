package com.feng;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.feng.dto.CUser;
import com.feng.dto.LoginRequestDto;
import com.feng.model.User;
import com.feng.model.Userl;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
class Springboot10DemoApplicationTests {

    @Autowired
    ObjectMapper mapper;
    @Test
    void tesr() throws JsonProcessingException {
      /*  JsonIgnoreProperties(ignoreUnknown = true) //必须，否则传入值多一个字段json转换就抛异常，登录就失败*/
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setEmail("weqwe@mail.com");
        String s = mapper.writeValueAsString(loginRequestDto);
        System.out.println(s);
        String ba="{\"email\":\"weqwe@mail.com\",\"password\":null,\"accept\":null,\"systemKey\":null,\"systemVersion\":null,\"device\":null,\"deviceId\":null,\"deviceInfo\":null,\"realIp\":null,\"realIp1\":null}";
        LoginRequestDto loginRequestDto1 = mapper.readValue(ba, LoginRequestDto.class);
        System.out.println(loginRequestDto1);

        String bookJson3 = "{\"name\":\"d2\", \"price\":\"1\", \"sn\":\"222\"}";
        Book b3 = mapper.readValue(bookJson3, Book.class);
        System.out.println(b3);
    }
    static class Book {
        String name;
        String price;
    }
        @Test
    void contextLoads() {
//        StringUtils.isEmpty("");
//        StringUtils.is
//        1. public static boolean isEmpty(String str)
//        判断某字符串是否为空，为空的标准是 str==null 或 str.length()==
//        StringUtils.isEmpty(null) = true
//        StringUti
//        StringUtils.isEmptls.isEmpty("") = true
//        StringUtils.isEmpty(" ") = false //注意在 StringUtils 中空格作非空处理
//        StringUtils.isEmpty("   ") = falsey("bob") = false
//        StringUtils.isEmpty(" bob ") = false
//        3. public static boolean isBlank(String str)
//        判断某字符串是否为空或长度为0或由空白符(whitespace) 构成
//        下面是示例：
//        StringUtils.isBlank(null) = true
//        StringUtils.isBlank("") = true
//        StringUtils.isBlank(" ") = true
//        StringUtils.isBlank("        ") = true
//        StringUtils.isBlank("\t \n \f \r") = true   //对于制表符、换行符、换页符和回车符
//
//        StringUtils.isBlank()   //均识为空白符
//        StringUtils.isBlank("\b") = false   //"\b"为单词边界符
//        StringUtils.isBlank("bob") = false
//        StringUtils.isBlank(" bob ") = false
    }

    @Test
    void test2() throws JsonProcessingException {
        List<CUser> cUser = new ArrayList<>();
        User user = new User("username", "password");
        Userl email = new Userl("11");
        List<User> userList=new ArrayList<>();
        userList.add(user);
        ModelMapper modelMapper=new     ModelMapper();
        List<CUser> userDTOS = modelMapper.map(userList,new TypeToken<List<CUser>>() {}.getType());
/*        modelMapper.map(userList, cUser);*/
     /*   modelMapper.map(email, cUser);*/
        System.out.println(cUser);
        System.out.println(userDTOS);
    }

    @Test
    void test3() {
        String format="";

        long n = 1516256119;
        long ns = 1625824825;
        long now = new Date().getTime() / 1000;
        System.out.println(now);
    }
}
