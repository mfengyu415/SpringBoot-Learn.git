package com.feng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot11DiystarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot11DiystarterApplication.class, args);
    }

}
